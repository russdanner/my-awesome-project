def result = [:]
result.sum = 10+10

def queryStatement = '*:*'

def query = searchService.createQuery()
query = query.setQuery(queryStatement)

def executedQuery = searchService.search(query)
def itemsFound = executedQuery.response.numFound
def items = executedQuery.response.documents

result.items = items

return result